package fr.uca.iut.clfreville2.persistence;

import fr.uca.iut.clfreville2.model.SensorRegistry;
import fr.uca.iut.clfreville2.model.sensor.AutoSensor;
import fr.uca.iut.clfreville2.model.sensor.VirtualSensor;
import fr.uca.iut.clfreville2.model.sensor.auto.RandomVariationStrategy;

public class StubSensorRegistryLoader implements SensorRegistryLoader {
    @Override
    public SensorRegistry load() {
        SensorRegistry registry = new SensorRegistry();
        registry.createManual("Manual 1");
        registry.createManual("Manual 2");
        AutoSensor autoSensor = registry.createAuto("Random 1", new RandomVariationStrategy(5));
        VirtualSensor virtualSensor = registry.createVirtual("Virtual 1");
        virtualSensor.addSource(autoSensor, 1.0D);
        return registry;
    }
}
