package fr.uca.iut.clfreville2.persistence;

import fr.uca.iut.clfreville2.model.SensorRegistry;

@FunctionalInterface
public interface SensorRegistryLoader {

    /**
     * Load the registry.
     *
     * @return The loaded registry.
     */
    SensorRegistry load();
}
