package fr.uca.iut.clfreville2.persistence;

import fr.uca.iut.clfreville2.model.SensorRegistry;

@FunctionalInterface
public interface SensorRegistrySaver {

    /**
     * Save the registry.
     *
     * @param registry The registry to save.
     */
    void save(SensorRegistry registry);
}
