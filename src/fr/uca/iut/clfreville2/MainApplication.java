package fr.uca.iut.clfreville2;

import fr.uca.iut.clfreville2.gui.FXMLUtils;
import fr.uca.iut.clfreville2.gui.MainWindows;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainApplication extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLUtils.load(MainApplication.class.getResource("/windows/MainWindows.fxml"), new MainWindows(primaryStage));
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
