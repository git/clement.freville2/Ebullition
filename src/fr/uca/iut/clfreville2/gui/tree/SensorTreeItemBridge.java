package fr.uca.iut.clfreville2.gui.tree;

import fr.uca.iut.clfreville2.model.SensorRegistry;
import fr.uca.iut.clfreville2.model.sensor.Sensor;
import fr.uca.iut.clfreville2.model.sensor.VirtualSensor;
import javafx.collections.ListChangeListener;
import javafx.scene.control.TreeItem;

public final class SensorTreeItemBridge {

    private SensorTreeItemBridge() {}

    public static TreeItem<Sensor> create(Sensor sensor) {
        TreeItem<Sensor> item = new TreeItem<>(sensor);
        if (sensor instanceof VirtualSensor virtual) {
            addChildren(virtual.sourcesProperty(), item);
            virtual.sourcesProperty().addListener(new ListChangeListener<VirtualSensor.DataSource>() {
                @Override
                public void onChanged(Change<? extends VirtualSensor.DataSource> change) {
                    while (change.next()) {
                        if (change.wasPermutated()) {
                            throw new UnsupportedOperationException();
                        } else if (change.wasUpdated()) {
                            throw new UnsupportedOperationException();
                        } else {
                            addChildren(change.getAddedSubList(), item);
                            removeChildren(change.getRemoved(), item);
                        }
                    }
                }
            });
        }
        return item;
    }

    public static TreeItem<Sensor> create(SensorRegistry registry) {
        TreeItem<Sensor> root = new TreeItem<>();
        for (Sensor sensor : registry) {
            root.getChildren().add(create(sensor));
        }
        registry.sensorsProperty().addListener(new ListChangeListener<Sensor>() {
            @Override
            public void onChanged(Change<? extends Sensor> change) {
                while (change.next()) {
                    if (change.wasPermutated()) {
                        throw new UnsupportedOperationException();
                    } else if (change.wasUpdated()) {
                        throw new UnsupportedOperationException();
                    } else {
                        addSensors(change.getAddedSubList(), root);
                        removeSensors(change.getRemoved(), root);
                    }
                }
            }
        });
        return root;
    }

    private static void addSensors(Iterable<? extends Sensor> sensors, TreeItem<Sensor> item) {
        for (Sensor sensor : sensors) {
            item.getChildren().add(create(sensor));
        }
    }

    private static void addChildren(Iterable<? extends VirtualSensor.DataSource> sources, TreeItem<Sensor> item) {
        for (VirtualSensor.DataSource source : sources) {
            item.getChildren().add(create(source.sensor()));
        }
    }

    private static void removeChildren(Iterable<? extends VirtualSensor.DataSource> sources, TreeItem<Sensor> item) {
        for (VirtualSensor.DataSource source : sources) {
            item.getChildren().removeIf(child -> child.getValue().equals(source.sensor()));
        }
    }

    private static void removeSensors(Iterable<? extends Sensor> sensors, TreeItem<Sensor> item) {
        for (Sensor sensor : sensors) {
            item.getChildren().removeIf(child -> child.getValue().equals(sensor));
        }
    }
}
