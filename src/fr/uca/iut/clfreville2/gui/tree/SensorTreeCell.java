package fr.uca.iut.clfreville2.gui.tree;

import fr.uca.iut.clfreville2.gui.image.ImageSupplier;
import fr.uca.iut.clfreville2.model.sensor.Sensor;
import javafx.scene.control.TreeCell;
import javafx.scene.image.ImageView;

public class SensorTreeCell extends TreeCell<Sensor> {

    private final ImageView imageView = new ImageView();

    private final ImageSupplier imageSupplier;

    public SensorTreeCell(ImageSupplier imageSupplier) {
        this.imageSupplier = imageSupplier;
    }

    @Override
    protected void updateItem(Sensor item, boolean empty) {
        super.updateItem(item, empty);
        if (empty || item == null) {
            textProperty().unbind();
            textProperty().setValue(null);
            setGraphic(null);
        } else {
            imageView.setImage(imageSupplier.apply(item));
            textProperty().bind(item.displayNameExpression());
            setGraphic(imageView);
        }
    }
}
