package fr.uca.iut.clfreville2.gui.list;

import fr.uca.iut.clfreville2.model.shared.Nameable;
import javafx.util.StringConverter;

public class NameableStringConverter<T extends Nameable> extends StringConverter<T> {

    private final Iterable<T> items;

    public NameableStringConverter(Iterable<T> items) {
        this.items = items;
    }

    @Override
    public String toString(T object) {
        return object.getName();
    }

    @Override
    public T fromString(String string) {
        for (T item : items) {
            if (item.getName().equals(string)) {
                return item;
            }
        }
        return null;
    }
}
