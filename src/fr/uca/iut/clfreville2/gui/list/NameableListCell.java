package fr.uca.iut.clfreville2.gui.list;

import fr.uca.iut.clfreville2.model.shared.ObservableIdentifiable;
import javafx.scene.control.ListCell;

public class NameableListCell<T extends ObservableIdentifiable> extends ListCell<T> {

    @Override
    protected void updateItem(T item, boolean empty) {
        super.updateItem(item, empty);
        if (empty || item == null) {
            textProperty().unbind();
        } else {
            textProperty().bind(item.displayNameExpression());
        }
    }
}
