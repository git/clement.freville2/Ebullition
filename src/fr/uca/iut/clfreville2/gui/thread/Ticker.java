package fr.uca.iut.clfreville2.gui.thread;

import fr.uca.iut.clfreville2.model.shared.Tickable;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;

public class Ticker extends Thread {

    private final Tickable tickable;
    private final DoubleProperty delay = new SimpleDoubleProperty(1D);
    private final BooleanProperty running = new SimpleBooleanProperty(true);

    public Ticker(Tickable tickable) {
        this.tickable = tickable;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep((int) (delay.getValue() * 1000));
                if (running.get()) {
                    Platform.runLater(tickable::tick);
                }
            } catch (InterruptedException e) {
                break;
            }
        }
    }

    public void setRunning(boolean running) {
        this.running.setValue(running);
    }

    public boolean getRunning() {
        return running.getValue();
    }

    public BooleanProperty runningProperty() {
        return running;
    }

    public void setDelay(double delay) {
        this.delay.setValue(delay);
    }

    public double getDelay() {
        return delay.getValue();
    }

    public DoubleProperty delayProperty() {
        return delay;
    }
}
