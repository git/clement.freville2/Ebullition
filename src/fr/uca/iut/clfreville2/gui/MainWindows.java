package fr.uca.iut.clfreville2.gui;

import fr.uca.iut.clfreville2.gui.image.ImageSupplier;
import fr.uca.iut.clfreville2.gui.image.SensorTypeImageSupplier;
import fr.uca.iut.clfreville2.gui.image.StandardImageSupplier;
import fr.uca.iut.clfreville2.gui.list.NameableStringConverter;
import fr.uca.iut.clfreville2.gui.table.SourceTableRow;
import fr.uca.iut.clfreville2.gui.table.WeightSpinnerTableCell;
import fr.uca.iut.clfreville2.gui.thread.Ticker;
import fr.uca.iut.clfreville2.gui.tree.SensorTreeCell;
import fr.uca.iut.clfreville2.gui.tree.SensorTreeItemBridge;
import fr.uca.iut.clfreville2.model.SensorRegistry;
import fr.uca.iut.clfreville2.model.binding.ToBooleanBinding;
import fr.uca.iut.clfreville2.model.sensor.AutoSensor;
import fr.uca.iut.clfreville2.model.sensor.ManualSensor;
import fr.uca.iut.clfreville2.model.sensor.Sensor;
import fr.uca.iut.clfreville2.model.sensor.VirtualSensor;
import fr.uca.iut.clfreville2.model.sensor.auto.AutoUpdateStrategy;
import fr.uca.iut.clfreville2.model.sensor.auto.factory.AutoUpdateStrategyFactory;
import fr.uca.iut.clfreville2.model.sensor.auto.factory.StandardUpdateStrategyFactory;
import fr.uca.iut.clfreville2.model.sensor.factory.AutoSensorFactory;
import fr.uca.iut.clfreville2.model.sensor.factory.ManualSensorFactory;
import fr.uca.iut.clfreville2.model.sensor.factory.SensorFactory;
import fr.uca.iut.clfreville2.model.sensor.factory.VirtualSensorFactory;
import fr.uca.iut.clfreville2.persistence.StubSensorRegistryLoader;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Slider;
import javafx.scene.control.Spinner;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class MainWindows {

    private final SensorRegistry registry = new StubSensorRegistryLoader().load();
    private final ImageSupplier imageSupplier = new StandardImageSupplier();
    private final ImageSupplier imageTypeSupplier = new SensorTypeImageSupplier();
    private final Ticker ticker;
    private final ModalFactory modalFactory;

    @FXML
    private TreeView<Sensor> sensorTree;

    @FXML
    private Text sensorId;

    @FXML
    public TextField sensorName;

    @FXML
    private Button changeBtn;

    @FXML
    private ChoiceBox<AutoUpdateStrategyFactory> autoType;
    private final ChangeListener<AutoUpdateStrategy> autoUpdateChangeHandler = this::changedUpdateStrategy;

    @FXML
    private Button visualizeBtn;

    @FXML
    private TableView<VirtualSensor.DataSource> sourcesView;

    @FXML
    private TableColumn<VirtualSensor.DataSource, Sensor> sourceIcon;

    @FXML
    private TableColumn<VirtualSensor.DataSource, VirtualSensor.DataSource> sourceWeight;

    @FXML
    private TableColumn<VirtualSensor.DataSource, String> sourceId;

    @FXML
    private TextField newName;

    @FXML
    private ChoiceBox<SensorFactory> createType;

    @FXML
    private Button createButton;

    @FXML
    private Spinner<Double> updateInterval;

    @FXML
    private ToggleButton autoUpdate;

    public MainWindows(Stage primaryStage) {
        this.ticker = new Ticker(registry);
        this.modalFactory = new ModalFactory(primaryStage);
        primaryStage.setOnHidden((e) -> ticker.interrupt());
        ticker.start();
    }

    @FXML
    public void onChangeClick() {
        Sensor selected = getSelectedSensor();
        if (!(selected instanceof ManualSensor sensor)) {
            return;
        }
        modalFactory.createModal(selected, root -> {
            Slider slider = new Slider(-5, 30, sensor.getTemperature());
            slider.setBlockIncrement(0.5);
            slider.setMajorTickUnit(0.5);
            slider.setMinorTickCount(0);
            slider.setSnapToTicks(true);
            slider.valueProperty().bindBidirectional(sensor.temperatureProperty());
            root.getChildren().add(slider);
        }).show();
    }

    @FXML
    public void onVisualizeClick() {
        Sensor selected = getSelectedSensor();
        if (selected == null) {
            return;
        }
        modalFactory.createModal(selected, root -> {
            ImageView imageView = new ImageView();
            imageView.setPreserveRatio(true);
            imageView.setFitHeight(200);
            selected.temperatureProperty().addListener((observable, old, newValue) -> imageView.setImage(imageSupplier.apply(selected)));
            imageView.setImage(imageSupplier.apply(selected));
            root.getChildren().add(imageView);
        }).show();
    }

    @FXML
    public void onCreateClick() {
        if (createType.getSelectionModel().getSelectedItem() == null || newName.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Empty form", ButtonType.CANCEL);
            alert.showAndWait();
            return;
        }
        Sensor sensor = createType.getSelectionModel().getSelectedItem().create(registry, newName.getText());
        Sensor selected = getSelectedSensor();
        if (selected instanceof VirtualSensor virtual) {
            virtual.addSource(sensor, 1D);
        }
    }

    @FXML
    private void initialize() {
        bindSensorTree();
        bindActiveButtons();
        bindSources();
        bindProvidable();
        bindUpdate();
    }

    @FXML
    private void bindSensorTree() {
        sensorTree.setCellFactory(tree -> new SensorTreeCell(imageTypeSupplier));
        sensorTree.setRoot(SensorTreeItemBridge.create(registry));
        sensorTree.getSelectionModel().selectedItemProperty().addListener((list, oldValue, newValue) -> {
            if (oldValue != null && oldValue.getValue() != null) {
                sensorName.textProperty().unbindBidirectional(oldValue.getValue().nameProperty());
                sensorName.setText(null);
                sourcesView.itemsProperty().unbind();
                sourcesView.setItems(FXCollections.emptyObservableList());
                if (oldValue.getValue() instanceof AutoSensor auto) {
                    auto.updateStrategyProperty().removeListener(autoUpdateChangeHandler);
                }
            }
            if (newValue != null && newValue.getValue() != null) {
                sensorId.textProperty().bind(newValue.getValue().displayNameExpression());
                sensorName.textProperty().bindBidirectional(newValue.getValue().nameProperty());
                if (newValue.getValue() instanceof AutoSensor auto) {
                    auto.updateStrategyProperty().addListener(autoUpdateChangeHandler);
                    changedUpdateStrategy(null, null, auto.getUpdateStrategy());
                }
                if (newValue.getValue() instanceof VirtualSensor virtual) {
                    sourcesView.itemsProperty().bind(virtual.sourcesProperty());
                }
            } else {
                sensorId.textProperty().unbind();
                sensorId.setText(null);
            }
        });
    }

    @FXML
    private void bindActiveButtons() {
        changeBtn.visibleProperty().bind(new ToBooleanBinding<>(
                sensorTree.getSelectionModel().selectedItemProperty(),
                treeItem -> treeItem != null && treeItem.getValue() instanceof ManualSensor
        ));
        visualizeBtn.visibleProperty().bind(sensorTree.getSelectionModel().selectedItemProperty().isNotNull());
        autoType.visibleProperty().bind(new ToBooleanBinding<>(
                sensorTree.getSelectionModel().selectedItemProperty(),
                treeItem -> treeItem != null && treeItem.getValue() instanceof AutoSensor
        ));
    }

    @FXML
    private void bindSources() {
        sourcesView.setRowFactory(row -> new SourceTableRow(this::delete));
        sourceWeight.setCellValueFactory(cell -> new SimpleObjectProperty<>(cell.getValue()));
        sourceWeight.setCellFactory(cell -> new WeightSpinnerTableCell());
        sourceIcon.setCellValueFactory(cell -> new SimpleObjectProperty<>(cell.getValue().sensor()));
        sourceIcon.setCellFactory(cell -> new TableCell<>() {
            @Override
            protected void updateItem(Sensor item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setGraphic(null);
                } else {
                    setGraphic(new ImageView(imageTypeSupplier.apply(item)));
                }
            }
        });
        sourceId.setCellValueFactory(cell -> cell.getValue().sensor().idProperty().asString());
    }

    @FXML
    private void bindProvidable() {
        autoType.getItems().addAll(StandardUpdateStrategyFactory.values());
        autoType.setConverter(new NameableStringConverter<>(autoType.getItems()));
        autoType.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            Sensor selected = getSelectedSensor();
            if (!(selected instanceof AutoSensor sensor)) {
                return;
            }
            sensor.setUpdateStrategy(newValue.create());
        });

        createType.getItems().addAll(
                new ManualSensorFactory(),
                new AutoSensorFactory(),
                new VirtualSensorFactory()
        );
        createType.setConverter(new NameableStringConverter<>(createType.getItems()));
        createType.getSelectionModel().selectFirst();
    }

    @FXML
    private void bindUpdate() {
        updateInterval.getValueFactory().valueProperty().bindBidirectional(ticker.delayProperty().asObject());
        autoUpdate.selectedProperty().bindBidirectional(ticker.runningProperty());
    }

    private Sensor getSelectedSensor() {
        TreeItem<Sensor> selected = sensorTree.getSelectionModel().getSelectedItem();
        if (selected == null) {
            return null;
        }
        return selected.getValue();
    }

    private void delete(Sensor source) {
        Sensor target = getSelectedSensor();
        if (!(target instanceof VirtualSensor virtual)) {
            return;
        }
        virtual.removeSource(source);
    }

    private void changedUpdateStrategy(ObservableValue<? extends AutoUpdateStrategy> ___, AutoUpdateStrategy o, AutoUpdateStrategy n) {
        autoType.getSelectionModel().select(n.getType());
    }
}
