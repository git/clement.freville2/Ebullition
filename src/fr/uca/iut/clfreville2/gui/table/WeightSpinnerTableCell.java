package fr.uca.iut.clfreville2.gui.table;

import fr.uca.iut.clfreville2.model.sensor.VirtualSensor;
import javafx.beans.property.ObjectProperty;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TableCell;

public class WeightSpinnerTableCell extends TableCell<VirtualSensor.DataSource, VirtualSensor.DataSource> {

    private final Spinner<Double> weight = new Spinner<>(new SpinnerValueFactory.DoubleSpinnerValueFactory(0.0D, 10.0));
    private ObjectProperty<Double> weightValueObj;

    @Override
    protected void updateItem(VirtualSensor.DataSource item, boolean empty) {
        super.updateItem(item, empty);
        if (empty) {
            if (weightValueObj != null) {
                weight.getValueFactory().valueProperty().unbindBidirectional(weightValueObj);
            }
            setGraphic(null);
        } else {
            weightValueObj = item.weightProperty().asObject();
            weight.getValueFactory().valueProperty().bindBidirectional(weightValueObj);
            setGraphic(weight);
        }
    }
}
