package fr.uca.iut.clfreville2.gui.table;

import fr.uca.iut.clfreville2.model.sensor.Sensor;
import fr.uca.iut.clfreville2.model.sensor.VirtualSensor.DataSource;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableRow;

import java.util.function.Consumer;

public class SourceTableRow extends TableRow<DataSource> {

    private final MenuItem deleteItem;
    private final ContextMenu menu;
    private final Consumer<Sensor> deleteConsumer;

    public SourceTableRow(Consumer<Sensor> deleteConsumer) {
        this.deleteItem = new MenuItem("Remove");
        this.menu = new ContextMenu(deleteItem);
        this.deleteConsumer = deleteConsumer;
    }

    @Override
    protected void updateItem(DataSource item, boolean empty) {
        super.updateItem(item, empty);
        if (empty) {
            deleteItem.setOnAction(null);
            setContextMenu(null);
        } else {
            deleteItem.setOnAction((e) -> deleteConsumer.accept(item.sensor()));
            setContextMenu(menu);
        }
    }
}
