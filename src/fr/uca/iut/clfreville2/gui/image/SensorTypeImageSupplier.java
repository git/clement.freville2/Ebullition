package fr.uca.iut.clfreville2.gui.image;

import fr.uca.iut.clfreville2.model.sensor.Sensor;
import fr.uca.iut.clfreville2.model.sensor.VirtualSensor;
import javafx.scene.image.Image;

public class SensorTypeImageSupplier implements ImageSupplier {

    private final Image simpleIcon;
    private final Image multiIcon;

    public SensorTypeImageSupplier() {
        simpleIcon = new Image("/images/captor_icon.png");
        multiIcon = new Image("/images/multi_captor_icon.png");
    }

    @Override
    public Image apply(Sensor sensor) {
        if (sensor instanceof VirtualSensor) {
            return multiIcon;
        }
        return simpleIcon;
    }
}
