package fr.uca.iut.clfreville2.gui.image;

import fr.uca.iut.clfreville2.model.sensor.Sensor;
import javafx.scene.image.Image;

public class StandardImageSupplier implements ImageSupplier {
    private static final int HOT_THRESHOLD = 22;
    private static final int WARM_THRESHOLD = 0;

    private final Image hotImage;
    private final Image warmImage;
    private final Image coldImage;

    public StandardImageSupplier() {
        hotImage = new Image("/images/visualizer/hot.jpg");
        warmImage = new Image("/images/visualizer/warm.jpg");
        coldImage = new Image("/images/visualizer/cold.jpg");
    }

    @Override
    public Image apply(Sensor sensor) {
        if (sensor.getTemperature() >= HOT_THRESHOLD) {
            return hotImage;
        }
        if (sensor.getTemperature() >= WARM_THRESHOLD) {
            return warmImage;
        }
        return coldImage;
    }
}
