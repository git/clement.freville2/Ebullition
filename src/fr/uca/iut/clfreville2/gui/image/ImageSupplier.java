package fr.uca.iut.clfreville2.gui.image;

import fr.uca.iut.clfreville2.model.sensor.Sensor;
import javafx.scene.image.Image;

import java.util.function.Function;

/**
 * Determine the image to display for a given sensor.
 */
public interface ImageSupplier extends Function<Sensor, Image> {
}
