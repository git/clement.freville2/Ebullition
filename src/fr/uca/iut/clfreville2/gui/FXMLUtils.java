package fr.uca.iut.clfreville2.gui;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import java.io.IOException;
import java.net.URL;

public final class FXMLUtils {

    private FXMLUtils() {}

    public static Parent load(URL location, Object controller) throws IOException {
        FXMLLoader loader = new FXMLLoader(location);
        loader.setController(controller);
        return loader.load();
    }
}
