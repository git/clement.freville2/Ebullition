package fr.uca.iut.clfreville2.gui;

import fr.uca.iut.clfreville2.model.sensor.Sensor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.util.function.Consumer;

import static java.util.Objects.requireNonNull;

public class ModalFactory {

    private final Window owner;

    public ModalFactory(Window owner) {
        this.owner = requireNonNull(owner, "window owner");
    }

    public Stage createModal(Consumer<Pane> initializer) {
        Stage stage = new Stage();
        Button quit = new Button("Quit");
        Pane root = new VBox();
        initializer.accept(root);
        root.getChildren().add(quit);
        stage.setScene(new Scene(root));
        stage.initOwner(owner);
        quit.setOnAction((ev) -> stage.hide());
        return stage;
    }

    public Stage createModal(Sensor sensor, Consumer<Pane> initializer) {
        return createModal(root -> {
            Text name = new Text();
            name.textProperty().bind(sensor.displayNameExpression().concat(sensor.temperatureProperty().asString(" (%.1f°C)")));
            root.getChildren().add(name);
            initializer.accept(root);
        });
    }
}
