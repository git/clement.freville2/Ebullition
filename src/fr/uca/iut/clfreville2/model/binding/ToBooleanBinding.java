package fr.uca.iut.clfreville2.model.binding;

import javafx.beans.Observable;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.value.ObservableObjectValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.concurrent.Callable;
import java.util.function.Predicate;

/**
 * Permet de se bind sur une valeur booléenne calculée sur un objet.
 *
 * @param <T> Le type de l'objet observé.
 * @implNote Basé sur {@link javafx.beans.binding.Bindings#createBooleanBinding(Callable, Observable...)} et {@link javafx.beans.binding.Bindings#isNull(ObservableObjectValue)}.
 */
public class ToBooleanBinding<T> extends BooleanBinding {

    private final ObservableObjectValue<T> target;
    private final Predicate<T> predicate;

    public ToBooleanBinding(ObservableObjectValue<T> target, Predicate<T> predicate) {
        this.target = target;
        this.predicate = predicate;
        super.bind(target);
    }

    @Override
    public void dispose() {
        super.unbind(target);
    }

    @Override
    protected boolean computeValue() {
        return predicate.test(target.get());
    }

    @Override
    public ObservableList<?> getDependencies() {
        return FXCollections.singletonObservableList(target);
    }
}
