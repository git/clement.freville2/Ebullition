package fr.uca.iut.clfreville2.model.sensor.factory;

import fr.uca.iut.clfreville2.model.SensorRegistry;
import fr.uca.iut.clfreville2.model.sensor.Sensor;

public class VirtualSensorFactory implements SensorFactory {

    @Override
    public Sensor create(SensorRegistry registry, String name) {
        return registry.createVirtual(name);
    }

    @Override
    public String getName() {
        return "Virtual";
    }
}
