package fr.uca.iut.clfreville2.model.sensor.factory;

import fr.uca.iut.clfreville2.model.SensorRegistry;
import fr.uca.iut.clfreville2.model.sensor.Sensor;
import fr.uca.iut.clfreville2.model.shared.Nameable;

public interface SensorFactory extends Nameable {

    Sensor create(SensorRegistry registry, String name);
}
