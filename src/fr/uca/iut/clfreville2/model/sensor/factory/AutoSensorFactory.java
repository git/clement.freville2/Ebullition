package fr.uca.iut.clfreville2.model.sensor.factory;

import fr.uca.iut.clfreville2.model.SensorRegistry;
import fr.uca.iut.clfreville2.model.sensor.Sensor;
import fr.uca.iut.clfreville2.model.sensor.auto.RandomVariationStrategy;

public class AutoSensorFactory implements SensorFactory {

    @Override
    public Sensor create(SensorRegistry registry, String name) {
        return registry.createAuto(name, new RandomVariationStrategy(5));
    }

    @Override
    public String getName() {
        return "Auto";
    }
}
