package fr.uca.iut.clfreville2.model.sensor;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.ReadOnlyListProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import static java.util.Objects.requireNonNull;

public class VirtualSensor extends Sensor {

    private final ObservableList<DataSource> sources = FXCollections.observableArrayList();
    private final ListProperty<DataSource> sourcesProperty = new SimpleListProperty<>(sources);
    private final SimpleDoubleProperty temperature = new SimpleDoubleProperty();
    private final ChangeListener<Number> changeHandler = (s, old, next) -> compute();

    public VirtualSensor(int id, String name) {
        super(id, name);
    }

    public void addSource(Sensor sensor, double weight) {
        sources.add(new DataSource(requireNonNull(sensor, "sensor"), weight));
        sensor.temperatureProperty().addListener(changeHandler);
    }

    public boolean removeSource(Sensor sensor) {
        return sources.removeIf(source -> {
            if (source.sensor().equals(sensor)) {
                sensor.temperatureProperty().removeListener(changeHandler);
                return true;
            }
            return false;
        });
    }

    @Override
    public ReadOnlyDoubleProperty temperatureProperty() {
        return temperature;
    }

    public ReadOnlyListProperty<DataSource> sourcesProperty() {
        return sourcesProperty;
    }

    protected void compute() {
        temperature.set(sources.stream()
                .mapToDouble(sensor -> sensor.temperature() * sensor.weight())
                .sum() /
                sources.stream()
                        .mapToDouble(DataSource::weight)
                        .sum());
    }

    public record DataSource(Sensor sensor, DoubleProperty weightProperty) {

        public DataSource(Sensor sensor, double weight) {
            this(sensor, new SimpleDoubleProperty(weight));
        }

        public double temperature() {
            return sensor().getTemperature();
        }

        public double weight() {
            return weightProperty.doubleValue();
        }
    }
}
