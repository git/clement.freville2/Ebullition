package fr.uca.iut.clfreville2.model.sensor.auto;

import fr.uca.iut.clfreville2.model.sensor.AutoSensor;
import fr.uca.iut.clfreville2.model.sensor.auto.factory.AutoUpdateStrategyFactory;
import fr.uca.iut.clfreville2.model.sensor.auto.factory.StandardUpdateStrategyFactory;

import java.util.Random;

public class RandomUpdateStrategy implements AutoUpdateStrategy {

    private final Random random;
    private final double min;
    private final double max;

    public RandomUpdateStrategy(double min, double max) {
        this(new Random(), min, max);
    }

    public RandomUpdateStrategy(Random random, double min, double max) {
        if (min > max) {
            throw new IllegalArgumentException("min > max");
        }
        if (min < -273) {
            throw new IllegalArgumentException("min < 0K (-273°C)");
        }
        this.random = random;
        this.min = min;
        this.max = max;
    }

    @Override
    public double nextValue(AutoSensor currentState) {
        return random.nextDouble(min, max);
    }

    @Override
    public AutoUpdateStrategyFactory getType() {
        return StandardUpdateStrategyFactory.RANDOM_UPDATE;
    }
}
