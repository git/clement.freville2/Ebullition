package fr.uca.iut.clfreville2.model.sensor.auto;

import fr.uca.iut.clfreville2.model.sensor.AutoSensor;
import fr.uca.iut.clfreville2.model.sensor.auto.factory.AutoUpdateStrategyFactory;
import fr.uca.iut.clfreville2.model.sensor.auto.factory.StandardUpdateStrategyFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class CpuUpdateStrategy implements AutoUpdateStrategy {

    @Override
    public double nextValue(AutoSensor currentState) {
        try {
            //return Integer.parseInt(Files.readString(Path.of("/sys/devices/pci0000:00/0000:00:18.3/hwmon/hwmon1/temp1_input")).trim()) / 1000D;
            return Integer.parseInt(Files.readString(Path.of("/sys/class/thermal/thermal_zone0/temp")).trim()) / 1000D;
        } catch (IOException e) {
            return -1;
        }
    }

    @Override
    public AutoUpdateStrategyFactory getType() {
        return StandardUpdateStrategyFactory.CPU;
    }
}
