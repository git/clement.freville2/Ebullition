package fr.uca.iut.clfreville2.model.sensor.auto.factory;

import fr.uca.iut.clfreville2.model.sensor.auto.AutoUpdateStrategy;
import fr.uca.iut.clfreville2.model.sensor.auto.CpuUpdateStrategy;
import fr.uca.iut.clfreville2.model.sensor.auto.RandomUpdateStrategy;
import fr.uca.iut.clfreville2.model.sensor.auto.RandomVariationStrategy;

/**
 * Provide common update strategies.
 * <p>
 * This enum ensures that the same strategy is always returned with the same identity.
 */
public enum StandardUpdateStrategyFactory implements AutoUpdateStrategyFactory {
    RANDOM_UPDATE {
        @Override
        public AutoUpdateStrategy create() {
            return new RandomUpdateStrategy(-10, 40);
        }

        @Override
        public String getName() {
            return "Random";
        }
    },
    RANDOM_VARIATION {
        @Override
        public AutoUpdateStrategy create() {
            return new RandomVariationStrategy(5);
        }

        @Override
        public String getName() {
            return "Random variation";
        }
    },
    CPU {
        @Override
        public AutoUpdateStrategy create() {
            return new CpuUpdateStrategy();
        }

        @Override
        public String getName() {
            return "CPU";
        }
    }
}
