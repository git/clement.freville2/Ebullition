package fr.uca.iut.clfreville2.model.sensor.auto.factory;

import fr.uca.iut.clfreville2.model.sensor.auto.AutoUpdateStrategy;
import fr.uca.iut.clfreville2.model.shared.Nameable;

public interface AutoUpdateStrategyFactory extends Nameable {

    AutoUpdateStrategy create();
}
