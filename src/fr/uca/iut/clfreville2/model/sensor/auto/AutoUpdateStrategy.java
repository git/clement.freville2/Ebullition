package fr.uca.iut.clfreville2.model.sensor.auto;

import fr.uca.iut.clfreville2.model.sensor.AutoSensor;
import fr.uca.iut.clfreville2.model.sensor.auto.factory.AutoUpdateStrategyFactory;

public interface AutoUpdateStrategy {

    /**
     * Computes the next value of the sensor.
     *
     * @param currentState The current state of the sensor.
     * @return The next value of the sensor.
     */
    double nextValue(AutoSensor currentState);

    /**
     * Gets the type of this strategy.
     *
     * @return The type of this strategy.
     */
    AutoUpdateStrategyFactory getType();
}
