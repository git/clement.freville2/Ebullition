package fr.uca.iut.clfreville2.model.sensor.auto;

import fr.uca.iut.clfreville2.model.sensor.AutoSensor;
import fr.uca.iut.clfreville2.model.sensor.auto.factory.AutoUpdateStrategyFactory;
import fr.uca.iut.clfreville2.model.sensor.auto.factory.StandardUpdateStrategyFactory;

import java.util.Random;

public class RandomVariationStrategy implements AutoUpdateStrategy {

    private final Random random;
    private final double maxVariation;

    public RandomVariationStrategy(int maxVariation) {
        this(new Random(), maxVariation);
    }

    public RandomVariationStrategy(Random random, int maxVariation) {
        this.random = random;
        this.maxVariation = maxVariation;
    }

    @Override
    public double nextValue(AutoSensor currentState) {
        return currentState.getTemperature() + random.nextDouble(maxVariation * 2) - maxVariation;
    }

    @Override
    public AutoUpdateStrategyFactory getType() {
        return StandardUpdateStrategyFactory.RANDOM_VARIATION;
    }
}
