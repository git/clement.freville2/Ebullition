package fr.uca.iut.clfreville2.model.sensor;

import fr.uca.iut.clfreville2.model.sensor.auto.AutoUpdateStrategy;
import fr.uca.iut.clfreville2.model.shared.Tickable;
import javafx.beans.Observable;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;

import static java.util.Objects.requireNonNull;

/**
 * A sensor that automatically create new temperatures.
 */
public class AutoSensor extends Sensor implements Tickable {

    private final DoubleProperty temperature = new SimpleDoubleProperty();
    private final ObjectProperty<AutoUpdateStrategy> updateStrategy = new SimpleObjectProperty<>();

    public AutoSensor(int id, String name, AutoUpdateStrategy updateStrategy) {
        super(id, name);
        this.updateStrategy.set(requireNonNull(updateStrategy, "update strategy"));
    }

    @Override
    public void tick() {
        temperature.set(updateStrategy.get().nextValue(this));
    }

    @Override
    public ReadOnlyDoubleProperty temperatureProperty() {
        return temperature;
    }

    public AutoUpdateStrategy getUpdateStrategy() {
        return updateStrategy.get();
    }

    public void setUpdateStrategy(AutoUpdateStrategy updateStrategy) {
        this.updateStrategy.set(requireNonNull(updateStrategy, "update strategy"));
    }

    public ReadOnlyObjectProperty<AutoUpdateStrategy> updateStrategyProperty() {
        return updateStrategy;
    }
}
