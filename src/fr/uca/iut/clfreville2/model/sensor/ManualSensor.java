package fr.uca.iut.clfreville2.model.sensor;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

/**
 * A simple sensor implementation that can be used to manually set the temperature.
 */
public class ManualSensor extends Sensor {

    private final DoubleProperty temperature = new SimpleDoubleProperty();

    public ManualSensor(int id, String name) {
        super(id, name);
    }

    public DoubleProperty temperatureProperty() {
        return temperature;
    }
}
