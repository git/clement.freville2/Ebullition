package fr.uca.iut.clfreville2.model.sensor;

import fr.uca.iut.clfreville2.model.shared.TemperatureHolder;
import fr.uca.iut.clfreville2.model.shared.ObservableIdentifiable;
import javafx.beans.binding.DoubleExpression;
import javafx.beans.binding.StringExpression;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import static java.util.Objects.requireNonNull;

/**
 * A sensor base using properties.
 */
public abstract class Sensor implements ObservableIdentifiable, TemperatureHolder {

    private final IntegerProperty id = new SimpleIntegerProperty();
    private final StringProperty name = new SimpleStringProperty();
    private final StringExpression displayName = name.concat(" #").concat(id.asString());

    public Sensor(int id, String name) {
        this.id.set(id);
        this.name.set(requireNonNull(name, "name"));
    }

    /**
     * Get the value binding of this sensor.
     *
     * @return The value binding.
     */
    public abstract DoubleExpression temperatureProperty();

    @Override
    public double getTemperature() {
        return temperatureProperty().get();
    }

    @Override
    public ReadOnlyIntegerProperty idProperty() {
        return id;
    }

    @Override
    public StringProperty nameProperty() {
        return name;
    }

    @Override
    public StringExpression displayNameExpression() {
        return displayName;
    }
}
