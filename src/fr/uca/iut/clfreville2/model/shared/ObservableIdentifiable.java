package fr.uca.iut.clfreville2.model.shared;

import javafx.beans.binding.StringExpression;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.StringProperty;

/**
 * An identifiable object that can be observed.
 */
public interface ObservableIdentifiable extends Identifiable {

    /**
     * Get the id property of this object.
     *
     * @return The id property.
     * @see #getId()
     */
    ReadOnlyIntegerProperty idProperty();

    @Override
    default int getId() {
        return idProperty().get();
    }

    /**
     * Get the name property of this object.
     *
     * @return The name property.
     * @see #getName()
     */
    StringProperty nameProperty();

    @Override
    default String getName() {
        return nameProperty().get();
    }

    @Override
    default void setName(String name) {
        nameProperty().set(name);
    }

    /**
     * Get the display name of this object.
     *
     * @return The display name.
     * @see #getDisplayName()
     */
    default StringExpression displayNameExpression() {
        return nameProperty();
    }

    @Override
    default String getDisplayName() {
        return displayNameExpression().get();
    }
}
