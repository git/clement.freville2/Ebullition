package fr.uca.iut.clfreville2.model.shared;

/**
 * Something that has a human-friendly name.
 * <p>
 * This interface differs from {@link Object#toString()} in that it explicitly
 * states that the name is not for debugging purposes.
 */
public interface Nameable {

    /**
     * Get the name of this object.
     *
     * @return The name of this object.
     */
    String getName();

    /**
     * Get the display name of this object.
     *
     * @return The display name.
     */
    default String getDisplayName() {
        return getName();
    }
}
