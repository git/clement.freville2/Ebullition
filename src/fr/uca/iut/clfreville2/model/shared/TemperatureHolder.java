package fr.uca.iut.clfreville2.model.shared;

/**
 * An object that can have a numeric value.
 */
public interface TemperatureHolder {

    /**
     * Get the temperature of this object.
     *
     * @return The temperature.
     */
    double getTemperature();
}
