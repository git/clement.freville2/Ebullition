package fr.uca.iut.clfreville2.model.shared;

/**
 * An object that can be differentiated by an id.
 */
public interface Identifiable extends Nameable {

    /**
     * Get the id of this object.
     *
     * @return The id.
     */
    int getId();

    /**
     * Set the name of this object.
     *
     * @param name The name.
     */
    void setName(String name);
}
