package fr.uca.iut.clfreville2.model.shared;

@FunctionalInterface
public interface Tickable {

    /**
     * Tick the current object.
     */
    void tick();
}
