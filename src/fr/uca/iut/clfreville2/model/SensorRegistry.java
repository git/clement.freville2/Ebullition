package fr.uca.iut.clfreville2.model;

import fr.uca.iut.clfreville2.model.sensor.AutoSensor;
import fr.uca.iut.clfreville2.model.sensor.ManualSensor;
import fr.uca.iut.clfreville2.model.sensor.Sensor;
import fr.uca.iut.clfreville2.model.sensor.VirtualSensor;
import fr.uca.iut.clfreville2.model.sensor.auto.AutoUpdateStrategy;
import fr.uca.iut.clfreville2.model.shared.Tickable;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.Iterator;
import java.util.function.IntSupplier;

/**
 * A registry class to hold thermal sensors instances.
 */
public class SensorRegistry implements Tickable, Iterable<Sensor> {

    private final IntSupplier nextIdSupplier = new SequenceIntSupplier();
    private final ObservableList<Sensor> sensors = FXCollections.observableArrayList();
    private final ListProperty<Sensor> sensorsList = new SimpleListProperty<>(sensors);

    /**
     * Create a new manual sensor and add it to the registry.
     *
     * @param name The name of the sensor.
     * @return The sensor created.
     */
    public ManualSensor createManual(String name) {
        return register(new ManualSensor(nextIdSupplier.getAsInt(), name));
    }

    /**
     * Create a new auto sensor and add it to the registry.
     *
     * @param name The name of the sensor.
     * @param strategy The strategy to update the sensor.
     * @return The sensor created.
     */
    public AutoSensor createAuto(String name, AutoUpdateStrategy strategy) {
        return register(new AutoSensor(nextIdSupplier.getAsInt(), name, strategy));
    }

    /**
     * Create a new virtual sensor and add it to the registry.
     *
     * @param name The name of the sensor.
     * @return The sensor created.
     */
    public VirtualSensor createVirtual(String name) {
        return register(new VirtualSensor(nextIdSupplier.getAsInt(), name));
    }

    /**
     * Add a new sensor to the registry.
     *
     * @param sensor The sensor to add.
     * @return The sensor added.
     * @param <T> The type of sensor.
     */
    protected <T extends Sensor> T register(T sensor) {
        sensorsList.add(sensor);
        return sensor;
    }

    /**
     * Return a read-only list view of sensors.
     *
     * @return A list view of sensors.
     */
    public ObservableList<Sensor> getSensors() {
        return FXCollections.unmodifiableObservableList(sensors);
    }

    /**
     * Return the read-only list property of sensors.
     *
     * @return The list property of sensors.
     */
    public ReadOnlyListProperty<Sensor> sensorsProperty() {
        return sensorsList;
    }

    @Override
    public void tick() {
        for (Sensor sensor : sensors) {
            if (sensor instanceof Tickable tickable) {
                tickable.tick();
            }
        }
    }

    @Override
    public Iterator<Sensor> iterator() {
        return getSensors().iterator();
    }
}
