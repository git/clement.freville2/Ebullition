package fr.uca.iut.clfreville2.model;

import java.util.function.IntSupplier;

/**
 * A simple integer supplier that returns a sequence of integers.
 *
 * @implNote This class is not thread-safe.
 */
public class SequenceIntSupplier implements IntSupplier {

    private int current;

    @Override
    public int getAsInt() {
        return ++current;
    }
}
